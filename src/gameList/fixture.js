import React from 'react';
import Card from 'material-ui/Card';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import {Link,browserHistory} from 'react-router';


export default class fixture extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.location.state.group);

    }

    render() {

        return (
          <Card style={{marginBottom:20}} onClick={()=>this.changeRoute(this.state.groups[group])}>
            <div className="row">
              <div className="col-sm-6">
                <Typography variant="headline" gutterBottom style={{padding:10}}>
                  Group  {this.props.params.group}
                </Typography>
              </div>
              <div className="col-sm-6">
                <Button variant="raised" style={{marginTop:10,marginRight:10,float:'right'}} className="predict" onClick={()=>browserHistory.push('/prediction')}>
                  Predict Score
                </Button>
              </div>
            </div>
            <Table>

              <TableHead>
                <TableRow>
                  <TableCell>TEAMS</TableCell>
                  <TableCell numeric>MP</TableCell>
                  <TableCell numeric>W</TableCell>
                  <TableCell numeric>D</TableCell>
                  <TableCell numeric>L</TableCell>
                  <TableCell numeric>GF</TableCell>
                  <TableCell numeric>GA</TableCell>
                  <TableCell numeric>Pts</TableCell>
                </TableRow>

              </TableHead>
              <TableBody>
                {this.props.location.state.group.length?
                  this.props.location.state.group.map(
                    (team)=>
                      <TableRow>
                        <TableCell>{team.name}</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                        <TableCell numeric>0</TableCell>
                      </TableRow>
                  ):null}

              </TableBody>
            </Table>

          </Card>
        );
    }
}
