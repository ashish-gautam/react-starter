import React from 'react';
import Card from 'material-ui/Card';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Typography from 'material-ui/Typography';
import axios from 'axios';
import {Link,browserHistory} from 'react-router';


export default class group extends React.Component {
    constructor(props) {
        super(props);
        this.state={groups:[]}
        this.changeRoute=this.changeRoute.bind(this);

    }
    componentDidMount(){
      let self=this;
      axios.get('https://api.myjson.com/bins/m4gzj')
        .then(function (response) {
          console.log(response.data);
          self.setState({groups:response.data})
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  changeRoute(item,group){

    browserHistory.push({pathname: '/fixture/'+group, state: {group:item}});
  }

    render() {

        return (
            <div>
              <Typography variant="headline" gutterBottom style={{padding:10}}>
                Russia World Cup Group
              </Typography>
              {Object.keys(this.state.groups).length?
                Object.keys(this.state.groups).map(
                (group)=>
                  <Card style={{marginBottom:20,cursor:'pointer'}} className="group" onClick={()=>this.changeRoute(this.state.groups[group],group)}>
                    <Typography variant="headline" gutterBottom style={{padding:10}}>
                      Group {group.toUpperCase()}
                    </Typography>
                    <Table>

                      <TableHead>
                        <TableRow>
                          <TableCell>TEAMS</TableCell>
                          <TableCell numeric>MP</TableCell>
                          <TableCell numeric>W</TableCell>
                          <TableCell numeric>D</TableCell>
                          <TableCell numeric>L</TableCell>
                          <TableCell numeric>GF</TableCell>
                          <TableCell numeric>GA</TableCell>
                          <TableCell numeric>Pts</TableCell>
                        </TableRow>

                      </TableHead>
                      <TableBody>
                        {this.state.groups[group].length?
                          this.state.groups[group].map(
                            (team)=>
                              <TableRow>
                                <TableCell>{team.name}</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                                <TableCell numeric>0</TableCell>
                              </TableRow>
                          ):null}

                      </TableBody>
                    </Table>

                  </Card>
              ):null}


            </div>
        );
    }
}
