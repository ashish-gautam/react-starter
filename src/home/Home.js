import React from "react";
import Card from "material-ui/Card";
import Typography from "material-ui/Typography";
import Color from "../utils/colors";
import menuItems from "../common/MenuItems";
import Avatar from "material-ui/Avatar";
import {Link,browserHistory} from 'react-router';


export default class Home extends React.Component {
  constructor(props) {
    super(props);


  }

  render() {

    return (
      <div>
        <div className="pad1" style={{paddingBottom:20}}>
          <div className="row pad">
            {menuItems.menus.length?
              menuItems.menus.map(
                (menu)=>
                  menu.text=='Home'?null:
                    <div className="col-sm-4 col-xs-12">
                      <Card className="row box-shadow" style={{margin:10,paddingLeft:15,paddingBottom:30,paddingTop:30,cursor:'pointer'}} onClick={()=>browserHistory.push(menu.link)}>
                        <div className="col-xs-1 col-sm-2 pad1">
                          <Avatar style={{height:50,width:50,color:Color.colorGreen,background:'#f9f9f9'}}>{menu.icon}</Avatar>
                        </div>
                        <div className="col-xs-11 col-sm-10">
                          <Typography variant="headline">{menu.text}</Typography>
                          <Typography variant="subheading" color="textSecondary">
                            {menu.subText}
                          </Typography>
                        </div>

                      </Card>
                    </div>
              ):null}
          </div>

        </div>

      </div>
    );
  }
}
