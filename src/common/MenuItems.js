import React from "react";
import Menu1 from "material-ui-icons/FormatAlignJustify";
import Menu2 from "material-ui-icons/Stars";
import Home from "material-ui-icons/Home";
import Menu3 from "material-ui-icons/Dashboard";
import Menu4 from "material-ui-icons/Games";


const menuItems = {

  menus: [
    {text: 'Home', icon: <Home/>, link: '/home',subText:'Home'},
    {text: 'React Life Cycle', icon: <Menu1/>, link: '/lifeCycle',subText:'Life Cycle React Js'},
    {text: 'State and Props', icon: <Menu2/>, link: '/state-demo',subText:'Demonstration state and props'},
    {text: 'World Cup Group', icon: <Menu4/>, link: '/group',subText:'Group of Russia World Cup'},
    {text: 'Prediction System', icon: <Menu3/>, link: '/prediction',subText:'Russia World cup prediction system'},


  ],
}

export default menuItems;
