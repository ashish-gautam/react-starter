import React from 'react';
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import MenuItems from './MenuItems';
import { browserHistory } from 'react-router';
export const mainMenu = (
  <div>
    {MenuItems.menus.length?
    MenuItems.menus.map(
      (menu)=>
        <ListItem button onClick={()=>browserHistory.push(menu.link)}>
          <ListItemIcon>
            {menu.icon}
          </ListItemIcon>
          <ListItemText primary={menu.text} />
        </ListItem>
    ):null}

  </div>
);

