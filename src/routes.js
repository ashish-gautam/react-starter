import React from 'react';
import { Route } from 'react-router';
import App from './App';
import Home from './home/Home';
import Prediction from './PredictionSystem/Prediction';
import Fixture from './gameList/fixture';
import Group from './gameList/group';
import StateDemo from './demo/state';
import PropsDemo from './demo/props';
import LifeCyle from './demo/lifeCycle';
import test from './test'


export default (
  <Route>
    <Route path="/" component={App}>
      <Route path="home" components={Home}/>
      <Route path="prediction" components={Prediction}/>
      <Route path="fixture/:group" components={Fixture}/>
      <Route path="group" components={Group}/>
      <Route path="state-demo" components={StateDemo}/>
      <Route path="props-demo" components={PropsDemo}/>
      <Route path="lifeCycle" components={LifeCyle}/>
    </Route>
  </Route>
);
