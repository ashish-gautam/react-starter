import {PropTypes} from "react";
import {createMuiTheme} from "material-ui/styles";
import purple from 'material-ui/colors/purple';
import Button from 'material-ui/Button';




const theme = createMuiTheme({
  palette: {
    primary: { main: purple[500] }, // Purple and green play nicely together.
    secondary: { main: '#11cb5f' }, // This is just green.A700 as hex.
  },

});
theme.propTypes = {
  theme: PropTypes.element,

};

export default theme;
