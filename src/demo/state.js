import React from 'react';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import Add from 'material-ui-icons/Add';
import {Link,browserHistory} from 'react-router';



export default class state extends React.Component {
    constructor(props) {
        super(props);
        this.state={name:'',sendProps:[]};
        this.handleChange=this.handleChange.bind(this);
        this.changeRoute=this.changeRoute.bind(this);
        this.addToProps=this.addToProps.bind(this);

    }
  handleChange (event) {

    this.setState({
      'name': event.target.value,
    });
  }
  changeRoute(){
    browserHistory.push({pathname: '/props-demo/', state: {demo:this.state.sendProps}});
  }
  addToProps(){
    this.state.sendProps.push(this.state.name);
    this.setState({
      name:'',
      sendProps:this.state.sendProps
    })
  }


    render() {

        return (
            <div>
              <TextField
                id="name"
                label="Name"
                value={this.state.name}
                onChange={this.handleChange}
                margin="normal"
                style={{paddingRight:50}}
              />
              <Button variant="fab" color="primary" aria-label="add" onClick={()=>this.addToProps()}>
              <Add/>
            </Button>
              {this.state.sendProps.length?
              this.state.sendProps.map(
                (item)=>
                  <div>
                    {item}
                  </div>
              ):null}

              <Button variant="raised" color="primary" onClick={this.changeRoute} style={{margin:20}}>
                Send as Props to component
              </Button>
            </div>
        );
    }
}
