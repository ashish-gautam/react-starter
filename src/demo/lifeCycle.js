import React from 'react';


export default class lifeCycle extends React.Component {
    constructor(props) {
        super(props);
        alert("This is Constructor");

    }
  componentWillMount(){alert("This is componentWillMount");}
  componentDidMount(){alert("This is componentDidMount");}

    render() {

        return (
            <div>
              <p>Example Life Cycle</p>
              <p>Countructor is Called first</p>
              <p>componentWillMount is callled second</p>
              <p>componentDidMount is called third</p>
              <p>finally UI is rendred</p>
            </div>
        );
    }
}
