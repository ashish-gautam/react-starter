import React from 'react';
import Event from '../utils/Event.js'
import Card from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';

export default class Prediction extends React.Component {
  constructor(props) {
    super(props);
    this.state={event:'',start:false,goal:0, open: false,team:'',name:'',predictionList:[],winner:[],brazil:0,germany:0}
    this.handleChange=this.handleChange.bind(this);
    this.handleText=this.handleText.bind(this);
    this.handleClose=this.handleClose.bind(this);
    this.addPrediction=this.addPrediction.bind(this);
    this.getWinner=this.getWinner.bind(this);

  }
  handleChange  (event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleText (event) {

    this.setState({
      'name': event.target.value,
    });
  }
  handleClose() {
    this.setState({ open: false });
  }
  addPrediction(){
    var obj={
      "name":this.state.name,
      "team":this.state.team
    };
    this.state.predictionList.push(obj);
    this.setState({predictionList:this.state.predictionList,name:'',team:'',open:false})


  }

  getWinner(){
    var winners=[]
    this.state.predictionList.forEach(function (val,key) {
      if(val.team=='Brazil'){
        winners.push(val);
      }
    })
    console.log(winners);
    this.setState({winner:winners});
    console.log(this.state);``
  }

  render() {

    return (
      <div className="row">
        <div className="col-sm-4">
          <Card style={{padding:20,textAlign:'center'}} className="row">
            <div className="col-sm-6">
              <Typography variant="display2" gutterBottom>
                Brazil
              </Typography>
              <Typography variant="display4" gutterBottom style={{color:this.state.brazil==''?null:'#296019'}}>
                {this.state.brazil}
              </Typography>
            </div>
            <div className="col-sm-6">
              <Typography variant="display2" gutterBottom>
                Germany
              </Typography>
              <Typography variant="display4" gutterBottom style={{color:this.state.brazil==''?null:'#2845d3'}}>
                {this.state.germany}
              </Typography>
            </div>
            <Button variant="raised" color="secondary" sryle={{float:'right'}} onClick={()=>this.setState({brazil:3,germany:2})}>
              Result
            </Button>

          </Card>
        </div>
        <div className="col-sm-4">
          <Card style={{padding:20,textAlign:'center',marginLeft:5}}>
            <Button variant="raised" color="secondary" sryle={{float:'right'}} onClick={()=>this.setState({open:true})}>
              Add Prediction
            </Button>
            <List>
              {this.state.predictionList.length?
                this.state.predictionList.map(
                  (item)=>
                    <ListItem>
                      <Avatar>
                        {item.name[0]}
                      </Avatar>
                      <ListItemText primary={item.name} secondary={item.team} />
                    </ListItem>
                ):null}
            </List>
          </Card>
        </div>
        <div className="col-sm-4">
          <Card style={{padding:20,textAlign:'center',marginLeft:5}} className="row">
            <Button variant="raised" color="secondary" sryle={{float:'right'}} onClick={this.getWinner}>
              Announce Winner
            </Button>
            {this.state.winner.length?
              this.state.winner.map(
                (item)=>
                  <ListItem>
                    <Avatar>
                      {item.name[0]}
                    </Avatar>
                    <ListItemText primary={item.name} secondary={Event.gifts[Math.floor(Math.random() * Math.floor(5))]} />
                  </ListItem>
              ):null}

          </Card>
        </div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Your Prediction</DialogTitle>
          <DialogContent>
            <DialogContentText>
            Predict Score
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="email"
              fullWidth
              value={this.state.name}
              onChange={this.handleText}
            />
            <FormControl style={{width:'100%'}}>
              <InputLabel htmlFor="age-simple">Winner Team</InputLabel>
              <Select
                value={this.state.team}
                onChange={this.handleChange}
                inputProps={{
                  name: 'team',
                  id: 'age-simple',
                }}>
                <MenuItem value='Brazil'>Brazil</MenuItem>
                <MenuItem value='Germany'>Germany</MenuItem>
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.addPrediction} color="primary">
              Predict
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
