import React, {PropTypes} from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import withWidth from "material-ui/utils/withWidth";
import theme from "./theme-default";
import Header from "./common/Header";


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      navDrawerOpen: false
    };
    console.log(this.props);
  }


  render() {
    return (
      <MuiThemeProvider muiTheme={theme}>
        <div>
          <Header/>

          <div className="containerWrapper">
            {this.props.children}
          </div>

        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
};

export default withWidth()(App);
